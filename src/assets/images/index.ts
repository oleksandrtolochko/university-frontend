import BisnessmanIcon from './BisnessmanIcon.svg';
import FourBloxInBox from './FourBloxInBox.svg';
import LogoutIcon from './LogoutIcon.svg';
import PersonIcon from './PersonIcon.svg';
import ThreeCubesAccent from './ThreeCubesAccent.svg';
import ThreeCubesPrime from './ThreeCubesPrime.svg';
import Vuesax from './Vuesax.svg';
import document from './document.svg';
import bell from './bell.svg';
import redPoint from './redPoint.svg';
import avatar from './avatar.jpg';
import editBtn from './editBtn.svg';
import searchIcon from './searchIcon.svg';
import BisnessmanIconGrey from './BisnessmanIconGrey.svg';

export {
    BisnessmanIconGrey,
    BisnessmanIcon,
    FourBloxInBox,
    LogoutIcon,
    PersonIcon,
    ThreeCubesAccent,
    ThreeCubesPrime,
    Vuesax,
    document,
    bell,
    redPoint,
    avatar,
    editBtn,
    searchIcon,
};
