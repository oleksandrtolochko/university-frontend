import { ReactNode, useEffect } from 'react';
import { bell, redPoint, avatar } from '../../assets/images';
import styles from './header.module.scss';
import { useAppDispatch, useAppSelector } from '../../redux/hooks/hooks';
import { TCurrentUser, fetchCurrentUser } from '../../redux/user/userSlice';
import { RootState } from '../../redux/store';
import { generateClassNames } from '../../utils';
import classnames from 'classnames';

interface headerProps {
    title?: string;
    children?: ReactNode;
    className?: string[];
}

function Header({ title, children, className }: headerProps): JSX.Element {
    const currentUser: TCurrentUser = useAppSelector(
        (state: RootState) => state.user.user
    );
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchCurrentUser());
    }, [dispatch]);

    const updatedClassNames: string[] = generateClassNames(className, styles);

    const classNames: string = classnames(styles.header, ...updatedClassNames);

    return (
        <section className={classNames}>
            {children}
            {title && <p className={styles.title}>{title}</p>}
            <div className={styles.info}>
                <div className={styles.notification}>
                    <img src={bell} alt="notification bell" />
                    <img
                        className={styles.point}
                        src={redPoint}
                        alt="red point"
                    />
                </div>
                <img src={avatar} alt="avatar" />
                {currentUser && <p>{currentUser.username}</p>}
            </div>
        </section>
    );
}

export default Header;
