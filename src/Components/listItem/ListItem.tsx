import { CSSProperties, ReactNode } from 'react';
import styles from './listItem.module.scss';
interface IListItem {
    children: ReactNode;
    style?: CSSProperties;
}

function ListItem({ children, style }: IListItem) {
    return (
        <div style={style} className={styles.listItem}>
            {children}
        </div>
    );
}

export default ListItem;
