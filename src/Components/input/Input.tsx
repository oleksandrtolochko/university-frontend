import { FieldErrors, UseFormRegister } from 'react-hook-form';
import styles from './input.module.scss';
import { ErrorMessage } from '@hookform/error-message';

export interface ILabel {
    label: string;
    type?: string | undefined;
    placeholder?: string | undefined;
}

export type TInputs = {
    name?: string;
    surname?: string;
    email?: string;
    password?: string;
    confirmPassword?: string;
    file?: File;
};

export interface IInputOptions {
    [key: string]: TInputs;
}

interface InputProps {
    label: string;
    placeholder?: string;
    type?: string;
    register: UseFormRegister<IInputOptions>;
    required: boolean;
    errors?: FieldErrors;
    defaultValue?: string;
}

const Input: React.FC<InputProps> = ({
    label,
    placeholder,
    type,
    register,
    required,
    errors,
    defaultValue,
}): JSX.Element => {
    return (
        <div className={styles.box}>
            <label className={styles.label}>{label}</label>
            <input
                className={styles.input}
                type={type}
                placeholder={placeholder}
                defaultValue={defaultValue}
                {...register(label, {
                    required,
                })}
                name={label}
            />
            <ErrorMessage
                errors={errors}
                name={label}
                render={({ messages }) =>
                    messages &&
                    Object.entries(messages).map(([type, message]) => (
                        <p className="errorMsg" key={type}>
                            {message}
                        </p>
                    ))
                }
            />
        </div>
    );
};

export default Input;
