import { createPortal } from 'react-dom';
import styles from '../modal/modal.module.scss';
import { ReactNode, useEffect } from 'react';
import { generateClassNames } from '../../utils';
import classnames from 'classnames';

interface IModalProps {
    children: ReactNode;
    onClose: () => void;
    className?: string[];
    id?: number;
}

const modalRoot = document.querySelector('#modal-root');

const Modal = ({ children, onClose, className }: IModalProps) => {
    useEffect(() => {
        const handleEscape = (evt: { key: string }) => {
            if (evt.key === 'Escape') onClose();
        };
        document.addEventListener('keydown', handleEscape);

        return () => {
            document.removeEventListener('keydown', handleEscape);
        };
    }, [onClose]);

    const updatedClassNames = generateClassNames(className, styles);

    const classNames = classnames(styles.Modal, ...updatedClassNames);

    return createPortal(
        <div className={styles.Overlay}>
            <div className={classNames}>
                <button
                    className={styles.closeBtn}
                    type="button"
                    onClick={onClose}
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="40"
                        height="40"
                        viewBox="0 0 40 40"
                        fill="none"
                        className={styles.closeBtnIcon}
                    >
                        <path
                            d="M20 17.643L27.0711 10.5719L29.4281 12.9289L22.357 20L29.4281 27.0711L27.0711 29.4281L20 22.357L12.9289 29.4281L10.5719 27.0711L17.643 20L10.5719 12.9289L12.9289 10.5719L20 17.643Z"
                            fill="black"
                        />
                    </svg>
                </button>
                {children}
            </div>
        </div>,
        modalRoot as Element
    );
};

export default Modal;
