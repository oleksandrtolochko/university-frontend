import { useState } from 'react';
import styles from './checkbox.module.scss';
import { BsCheckSquare } from 'react-icons/bs';

interface InputProps {
    label: string;
    onToggle: (isChecked: boolean) => void;
}

const Checkbox: React.FC<InputProps> = ({ label, onToggle }): JSX.Element => {
    const [isChecked, setIsChecked] = useState(false);

    const handleChange = () => {
        setIsChecked((prev) => !prev);
        onToggle(!isChecked);
    };

    return (
        <div className={styles.wrapper}>
            <label className={styles.label}>
                <input
                    className={styles.input}
                    type="checkbox"
                    checked={isChecked}
                    onChange={handleChange}
                />
                {!isChecked ? (
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        className={styles.icon}
                    >
                        <path
                            d="M19.1429 21.5H4.85714C3.55533 21.5 2.5 20.4447 2.5 19.1429V4.85714C2.5 3.55533 3.55533 2.5 4.85714 2.5H19.1429C20.4447 2.5 21.5 3.55533 21.5 4.85714V19.1429C21.5 20.4447 20.4447 21.5 19.1429 21.5Z"
                            fill="white"
                            stroke="#CAD2D6"
                        />
                    </svg>
                ) : (
                    <BsCheckSquare className={styles.checked_icon} />
                )}
                <span>{label}</span>
            </label>
        </div>
    );
};
export default Checkbox;
