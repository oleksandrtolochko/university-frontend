import React, { Suspense } from 'react';
import Sidebar from '../sidebar/Sidebar';
import { Outlet } from 'react-router-dom';
import styles from './layout.module.scss';

function Layout(): JSX.Element {
    return (
        <section className={styles.layout}>
            <Sidebar />
            <Suspense fallback={null}>
                <Outlet />
            </Suspense>
        </section>
    );
}

export default Layout;
