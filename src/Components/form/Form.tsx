import { FormEvent, ReactNode } from 'react';
import { generateClassNames } from '../../utils';
import classnames from 'classnames';
import styles from './form.module.scss';

interface Props {
    children: ReactNode;
    className?: string[];
    onSubmit: (event: FormEvent<HTMLFormElement>) => void;
}

const Form: React.FC<Props> = ({
    children,
    className,
    onSubmit,
}): JSX.Element => {
    const updatedClassNames: string[] = generateClassNames(className, styles);

    const classNames: string = classnames(styles.default, ...updatedClassNames);

    return (
        <form className={classNames} onSubmit={onSubmit}>
            {children}
        </form>
    );
};

export default Form;
