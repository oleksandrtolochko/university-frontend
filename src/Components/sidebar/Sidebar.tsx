import Logo from '../logo/Logo';
import styles from './sidebar.module.scss';
import {
    BisnessmanIcon,
    FourBloxInBox,
    LogoutIcon,
    PersonIcon,
    Vuesax,
    document,
} from '../../assets/images';
import { NavLink, useNavigate } from 'react-router-dom';
import { userLogout } from '../../services/auth.service';

interface ISideTab {
    name: string;
    icon: string;
}

const sideTabs: ISideTab[] = [
    { name: 'dashboard', icon: FourBloxInBox },
    { name: 'courses', icon: Vuesax },
    { name: 'lectors', icon: BisnessmanIcon },
    { name: 'groups', icon: document },
    { name: 'students', icon: PersonIcon },
];

function Sidebar() {
    const navigate = useNavigate();

    const handleLogout = () => {
        userLogout();
        navigate('/sign-in');
    };

    return (
        <section className={styles.sidebar}>
            <Logo variant="accent" className={['sidebarlogo']} />
            <div>
                {sideTabs && (
                    <div className={styles.tablist}>
                        {sideTabs.map((tab) => (
                            <NavLink
                                to={tab.name}
                                key={tab.name}
                                className={styles.tab}
                            >
                                {({ isActive, isPending }) => (
                                    <div
                                        className={
                                            isActive ? `${styles.active}` : ''
                                        }
                                        style={{ display: 'flex' }}
                                    >
                                        <img
                                            className={styles.icon}
                                            src={tab.icon}
                                            alt={tab.name}
                                        />
                                        <div>{tab.name}</div>
                                    </div>
                                )}
                            </NavLink>
                        ))}
                    </div>
                )}
                <div className={styles.logout} onClick={handleLogout}>
                    <img
                        className={styles.logout__icon}
                        src={LogoutIcon}
                        alt="logout"
                    />
                    <div>logout</div>
                </div>
            </div>
        </section>
    );
}

export default Sidebar;
