import styles from './button.module.scss';

import { MouseEventHandler, ReactNode } from 'react';
import classnames from 'classnames';
import { generateClassNames } from '../../utils';

interface Props {
    children: ReactNode;
    className?: string[];
    type?: 'button' | 'submit' | 'reset';
    onClick?: MouseEventHandler<HTMLButtonElement>;
    disabled?: boolean;
}

const Button: React.FC<Props> = ({
    children,
    className,
    type = 'button',
    onClick,
    disabled = false,
}): JSX.Element => {
    const updatedClassNames = generateClassNames(className, styles);

    const classNames = classnames(styles.default, ...updatedClassNames);

    return (
        <button
            className={classNames}
            type={type}
            onClick={onClick}
            disabled={disabled}
        >
            {children}
        </button>
    );
};

export default Button;
