import { ThreeCubesAccent, ThreeCubesPrime } from '../../assets/images';
import styles from './logo.module.scss';
import classnames from 'classnames';
import { generateClassNames } from '../../utils';

interface Props {
    className?: string[];
    variant: 'primary' | 'accent';
    width?: number;
    height?: number;
}

const Logo: React.FC<Props> = ({
    className,
    variant,
    width,
    height,
}): JSX.Element => {
    const updatedClassNames: string[] = generateClassNames(className, styles);

    const classNames: string = classnames(styles.logo, ...updatedClassNames);

    const Logotype = variant === 'primary' ? ThreeCubesPrime : ThreeCubesAccent;

    return (
        <div className={classNames}>
            <img src={Logotype} alt="Logo" width={width} height={height} />
        </div>
    );
};

export default Logo;
