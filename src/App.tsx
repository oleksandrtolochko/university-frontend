import { Route, Routes } from 'react-router-dom';
import {
    Login,
    Register,
    ForgotPassword,
    ResetPassword,
    Lectors,
    Groups,
    Courses,
    Students,
    StudentDetails,
} from './Pages';
import { Layout } from './Components';
import ProtectedRoute from './Routes/ProtectedRoute';
import PublicRoute from './Routes/PublicRoute';

function App() {
    return (
        <Routes>
            <Route element={<PublicRoute />}>
                <Route path="sign-in" element={<Login />} />
                <Route path="sign-up" element={<Register />} />
                <Route path="forgot-password" element={<ForgotPassword />} />
                <Route path="reset-password" element={<ResetPassword />} />
            </Route>

            <Route element={<ProtectedRoute />}>
                <Route path="/layout" element={<Layout />}>
                    <Route path="lectors" element={<Lectors />} />
                    <Route path="groups" element={<Groups />} />
                    <Route path="courses" element={<Courses />} />
                    <Route path="students" element={<Students />} />
                </Route>
                <Route path="students/:id" element={<StudentDetails />} />
            </Route>
        </Routes>
    );
}

export default App;
