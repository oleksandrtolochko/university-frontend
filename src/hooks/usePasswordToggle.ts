import { useState } from 'react';

export const usePasswordToggle = (initialValue = false) => {
    const [isShowPass, setIsShowPass] = useState(initialValue);

    const handleToggle = (data: boolean) => {
        if (data) {
            setIsShowPass(true);
        } else {
            setIsShowPass(false);
        }
    };

    return { isShowPass, handleToggle };
};
