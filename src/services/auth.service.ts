import axios from 'axios';
import { redirect } from 'react-router-dom';

// const API_URL = 'http://localhost:3003/api/v1';
const API_URL = 'https://university-test-test.onrender.com/api/v1';

const userRegistration = async (email: string, password: string) => {
    try {
        const res = await axios.post(`${API_URL}/auth/sign-up`, {
            email,
            password,
        });

        return res;
    } catch (error) {
        console.log(error);
    }
};

const userLogin = async (email: string, password: string) => {
    try {
        const res = await axios.post(`${API_URL}/auth/sign-in`, {
            email,
            password,
        });

        if (res.data.accessToken) {
            localStorage.setItem(
                'accessToken',
                JSON.stringify(res.data.accessToken)
            );
        }
        return res;
    } catch (error) {
        console.log(error);
    }
};

const userLogout = () => {
    localStorage.removeItem('accessToken');
};

const getCurrentUser = async () => {
    try {
        const res = await axios.get(`${API_URL}/auth/profile`, {
            headers: {
                Authorization: `Bearer ${JSON.parse(
                    localStorage.getItem('accessToken') as string
                )}`,
            },
        });

        return res.data;
    } catch (error) {
        localStorage.removeItem('accessToken');
        redirect('/sign-in');
    }
};

export { userRegistration, userLogin, userLogout, getCurrentUser };
