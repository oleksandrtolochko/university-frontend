import axios from 'axios';
import { ILector } from '../redux/lectors/lectorsSlice';

// const API_URL = 'http://localhost:3003/api/v1/lectors';
const API_URL = 'https://university-test-test.onrender.com/api/v1/lectors';

const getLectorsList: () => Promise<ILector[] | undefined> = async () => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
        localStorage.getItem('accessToken') as string
    )}`;
    try {
        const res = await axios.get(`${API_URL}`);

        if (Array.isArray(res.data)) {
            return res.data as ILector[];
        }
    } catch (error) {
        console.log(error);
    }
};

export interface ILectorCreate {
    name: string;
    surname: string;
    email: string;
    password: string;
}

const lectorCreate = async (data: ILectorCreate) => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
        localStorage.getItem('accessToken') as string
    )}`;
    try {
        const res = await axios.post(`${API_URL}`, data);
        return res.data;
    } catch (error) {
        console.log(error);
    }
};

export interface ILectorUpdate {
    name?: string;
    surname?: string;
    email?: string;
    password?: string;
}

const lectorUpdate = async (id: number, data: ILectorUpdate) => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
        localStorage.getItem('accessToken') as string
    )}`;
    try {
        const res = await axios.patch(`${API_URL}/${id}`, data);
        return res.data;
    } catch (error) {
        console.log(error);
    }
};

export { getLectorsList, lectorCreate, lectorUpdate };
