import axios from 'axios';
import { IGroup } from '../redux/groups/groupsSlice';

// const API_URL = 'http://localhost:3003/api/v1/groups';
const API_URL = 'https://university-test-test.onrender.com/api/v1/groups';

export interface IGroupCreate {
    name: string;
}
export interface IGroupUpdate {
    name?: string;
}

export const groupsService = {
    getGroupsList: async () => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.get(`${API_URL}`);

            if (Array.isArray(res.data)) {
                return res.data as IGroup[];
            }
        } catch (error) {
            console.log(error);
        }
    },

    groupCreate: async (data: IGroupCreate) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.post(`${API_URL}`, data);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },

    groupUpdate: async (id: number, data: IGroupUpdate) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.patch(`${API_URL}/${id}`, data);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },
};
