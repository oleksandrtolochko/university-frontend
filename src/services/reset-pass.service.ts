import axios from 'axios';

// const API_URL = 'http://localhost:3003/api/v1/auth';
const API_URL = 'https://university-test-test.onrender.com/api/v1/auth';

const requestToChangePass = async (email: string) => {
    console.log('email: ', email);
    try {
        const res = await axios.post(`${API_URL}/forgot-password`, {
            email,
        });

        console.log('res: ', res);
        return res;
    } catch (error) {
        console.log(error);
    }
};

const changePass = async (token: string, newPassword: string) => {
    console.log('token: ', token);
    console.log('password: ', newPassword);
    try {
        const res = await axios.post(`${API_URL}/reset-password`, {
            token,
            newPassword,
        });

        console.log('res: ', res);
        return res;
    } catch (error) {
        console.log(error);
    }
};

// const userLogin = async (email: string, password: string) => {
//     try {
//         const res = await axios.post(`${API_URL}/auth/sign-in`, {
//             email,
//             password,
//         });

//         if (res.data.accessToken) {
//             localStorage.setItem(
//                 'accessToken',
//                 JSON.stringify(res.data.accessToken)
//             );
//         }
//         return res;
//     } catch (error) {
//         console.log(error);
//     }
// };

export { requestToChangePass, changePass };
