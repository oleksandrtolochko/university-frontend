import axios from 'axios';
import { IStudentQuery } from '../redux/students/studentsSlice';

// const API_URL = 'http://localhost:3003/api/v1/students';
const API_URL = 'https://university-test-test.onrender.com/api/v1/students';

export interface IStudent {
    id: number;
    name: string;
    surname: string;
    email: string;
    age: number;
    imagePath?: string;
    groupName?: string;
    createdAt?: Date;
    updatedAt?: Date;
    courseName?: string;
}

export interface IStudentCreate {
    name: string;
    surname: string;
    age: number;
    email: string;
    imagePath?: string;
    groupId?: number;
}
export interface IStudentUpdate {
    name?: string;
    surname?: string;
    age?: number;
    email?: string;
    imagePath?: string;
    groupId?: number;
}

export const studentsService = {
    getStudentsList: async (query: IStudentQuery) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            if (!query.search) {
                const res = await axios.get(
                    `${API_URL}?sortBy=${query.sortBy}`
                );
                if (Array.isArray(res.data)) {
                    return res.data as IStudent[];
                }
            } else {
                const res = await axios.get(
                    `${API_URL}?name=${query.search}&sortBy=${query.sortBy}`
                );
                if (Array.isArray(res.data)) {
                    return res.data as IStudent[];
                }
            }
        } catch (error) {
            console.log(error);
        }
    },

    getStudentById: async (id: number) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.get(`${API_URL}/${id}`);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },

    uploadPhoto: async (id: number, data: any) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.post(
                `${API_URL}/${id}/upload-photo`,
                data,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                }
            );

            return res.data;
        } catch (error) {
            console.log(error);
        }
    },

    getStudentImg: async (imagePath: string) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.get(`${API_URL}/photo/${imagePath}`, {
                responseType: 'blob',
            });
            return URL.createObjectURL(res.data);
        } catch (error) {
            console.log(error);
        }
    },

    studentCreate: async (data: IStudentCreate) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.post(`${API_URL}`, data);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },

    studentUpdate: async (id: number, data: IStudentUpdate) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.patch(`${API_URL}/${id}`, data);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },
};
