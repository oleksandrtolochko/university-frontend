import axios from 'axios';
import { ICourse } from '../redux/courses/coursesSlice';

// const API_URL = 'http://localhost:3003/api/v1/courses';
const API_URL = 'https://university-test-test.onrender.com/api/v1/courses';

export interface ICourseCreate {
    name: string;
    description: string;
    hours: string;
}

export interface ICourseUpdate {
    name?: string;
    description?: string;
    hours?: string;
}

export const coursesService = {
    getCoursesList: async () => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.get(`${API_URL}`);

            if (Array.isArray(res.data)) {
                return res.data as ICourse[];
            }
        } catch (error) {
            console.log(error);
        }
    },

    courseCreate: async (data: ICourseCreate) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.post(`${API_URL}`, data);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },

    courseUpdate: async (id: number, data: ICourseUpdate) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(
            localStorage.getItem('accessToken') as string
        )}`;
        try {
            const res = await axios.patch(`${API_URL}/${id}`, data);
            return res.data;
        } catch (error) {
            console.log(error);
        }
    },
};
