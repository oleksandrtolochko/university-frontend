import { Navigate, Outlet } from 'react-router-dom';

interface ProtectedRouteProps {
    redirectPath?: string;
}

const PublicRoute: React.FC<ProtectedRouteProps> = ({
    redirectPath = '/layout/lectors',
}) => {
    const token: string | null = JSON.parse(
        localStorage.getItem('accessToken') as string
    );

    if (token) {
        return <Navigate to={redirectPath} replace />;
    }

    return <Outlet />;
};

export default PublicRoute;
