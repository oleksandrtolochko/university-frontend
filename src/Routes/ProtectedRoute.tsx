import { Navigate, Outlet } from 'react-router-dom';

interface ProtectedRouteProps {
    // token: string | null;
    redirectPath?: string;
}

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({
    // token,
    redirectPath = '/sign-in',
}) => {
    const token: string | null = JSON.parse(
        localStorage.getItem('accessToken') as string
    );

    if (!token) {
        return <Navigate to={redirectPath} replace />;
    }

    return <Outlet />;
};

export default ProtectedRoute;
