import Joi from 'joi';

export const schema = {
    loginSchema: Joi.object({
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .required(),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(),
    }),

    registerSchema: Joi.object({
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .required(),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(),

        'confirm password': Joi.ref('password'),
    }),

    emailSchema: Joi.object({
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .required(),
    }),

    resetPassSchema: Joi.object({
        'new password': Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(),

        'confirm password': Joi.ref('new password'),
    }),

    searchSchema: Joi.object({
        search: Joi.string().required(),
    }),

    createLectorSchema: Joi.object({
        name: Joi.string().min(2).required(),
        surname: Joi.string().min(2).required(),
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .required(),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(),
    }),

    updateLectorSchema: Joi.object({
        name: Joi.string().min(2).empty(''),
        surname: Joi.string().min(2).empty(''),
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .empty(''),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .empty(''),
    }),

    createCourseSchema: Joi.object({
        name: Joi.string().min(2).required(),
        description: Joi.string().min(10).required(),
        hours: Joi.number().required(),
    }),

    updateCourseSchema: Joi.object({
        name: Joi.string().min(2).empty(''),
        description: Joi.string().min(10).empty(''),
        hours: Joi.number().empty(''),
    }),

    createGroupSchema: Joi.object({
        name: Joi.string().min(2).required(),
    }),
    updateGroupSchema: Joi.object({
        name: Joi.string().min(2).empty(''),
    }),

    createStudentSchema: Joi.object({
        name: Joi.string().min(2).required(),
        surname: Joi.string().min(2).required(),
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .required(),

        age: Joi.number().max(120).required(),
    }),

    updateStudentSchema: Joi.object({
        name: Joi.string().min(2).empty(''),
        surname: Joi.string().min(2).empty(''),
        email: Joi.string()
            .email({
                minDomainSegments: 2,
                tlds: { allow: ['com', 'net'] },
            })
            .empty(''),
        age: Joi.number().max(120).empty(''),
    }),
};
