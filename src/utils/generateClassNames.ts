import { CSSProperties } from 'react';

export const generateClassNames = (
    className?: string[],
    styles?: CSSProperties
): string[] => {
    return className && styles
        ? className.reduce((classNames, el) => {
              const styleValue = styles[el as keyof CSSProperties];
              if (typeof styleValue === 'string') {
                  classNames.push(styleValue);
              }
              return classNames;
          }, [] as string[])
        : [];
};
