import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {
    IGroupCreate,
    IGroupUpdate,
    groupsService,
} from '../../services/groups.service';

export interface IGroup {
    id: number;
    name: string;
    students: [];
    createdAt: Date;
    updatedAt: Date;
}

export interface IGroupsState {
    groups: IGroup[];
    status: 'idle' | 'loading' | 'fulfilled' | 'rejected';
    error: null | string | undefined;
}

export const fetchGroupsList = createAsyncThunk<
    IGroup[],
    undefined,
    { rejectValue: string }
>('groups/fetchGroupsList', async function (_, { rejectWithValue }) {
    try {
        const res = await groupsService.getGroupsList();
        return res as IGroup[];
    } catch (error) {
        return rejectWithValue('Failed to fetch groups list');
    }
});

export const createNewGroup = createAsyncThunk<
    IGroup,
    IGroupCreate,
    { rejectValue: string }
>(
    'groups/createNewGroup',
    async function (data: IGroupCreate, { rejectWithValue }) {
        try {
            const res = await groupsService.groupCreate(data);
            return res as IGroup;
        } catch (error) {
            return rejectWithValue('Failed to create new course');
        }
    }
);

export interface IGroupUpdateData {
    data: IGroupUpdate;
    group: IGroup;
}

export const updateGroup = createAsyncThunk<
    IGroup,
    IGroupUpdateData,
    { rejectValue: string }
>(
    'groups/updateGroup',
    async function (data: IGroupUpdateData, { rejectWithValue }) {
        try {
            const { name } = data.data;
            const id = data.group.id;
            const updateFields = { name };
            const res = await groupsService.groupUpdate(id, updateFields);
            return res as IGroup;
        } catch (error) {
            return rejectWithValue('Failed to create new lector');
        }
    }
);

const initialState = {
    groups: [],
    status: 'idle',
    error: null,
} as IGroupsState;

const groupsSlice = createSlice({
    name: 'groups',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchGroupsList.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(fetchGroupsList.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.groups = action.payload;
        });
        builder.addCase(fetchGroupsList.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(createNewGroup.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(createNewGroup.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.groups.push({ ...action.payload, students: [] });
        });
        builder.addCase(createNewGroup.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(updateGroup.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(updateGroup.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            const groupIndex = state.groups.findIndex(
                (course) => course.id === action.payload.id
            );
            if (groupIndex !== -1) {
                state.groups[groupIndex] = {
                    ...state.groups[groupIndex],
                    ...action.payload,
                };
            }
        });
        builder.addCase(updateGroup.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
    },
});

export default groupsSlice.reducer;
