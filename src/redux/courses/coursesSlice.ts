import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {
    ICourseCreate,
    ICourseUpdate,
    coursesService,
} from '../../services/courses.service';

export interface ICourse {
    id: number;
    name: string;
    description: string;
    hours: string;
    students: [];
    createdAt: Date;
    updatedAt: Date;
}

export interface ICoursesState {
    courses: ICourse[];
    status: 'idle' | 'loading' | 'fulfilled' | 'rejected';
    error: null | string | undefined;
}

export const fetchCoursesList = createAsyncThunk<
    ICourse[],
    undefined,
    { rejectValue: string }
>('courses/fetchCoursesList', async function (_, { rejectWithValue }) {
    try {
        const res = await coursesService.getCoursesList();
        return res as ICourse[];
    } catch (error) {
        return rejectWithValue('Failed to fetch courses list');
    }
});

export const createNewCourse = createAsyncThunk<
    ICourse,
    ICourseCreate,
    { rejectValue: string }
>(
    'courses/createNewCourse',
    async function (data: ICourseCreate, { rejectWithValue }) {
        try {
            const res = await coursesService.courseCreate(data);
            return res as ICourse;
        } catch (error) {
            return rejectWithValue('Failed to create new course');
        }
    }
);

export interface ICourseUpdateData {
    data: ICourseUpdate;
    course: ICourse;
}

export const updateCourse = createAsyncThunk<
    ICourse,
    ICourseUpdateData,
    { rejectValue: string }
>(
    'courses/updateCourse',
    async function (data: ICourseUpdateData, { rejectWithValue }) {
        try {
            const { name, description, hours } = data.data;
            const id = data.course.id;
            const updateFields = { name, description, hours };
            const res = await coursesService.courseUpdate(id, updateFields);
            return res as ICourse;
        } catch (error) {
            return rejectWithValue('Failed to create new lector');
        }
    }
);

const initialState = {
    courses: [],
    status: 'idle',
    error: null,
} as ICoursesState;

const coursesSlice = createSlice({
    name: 'courses',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchCoursesList.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(fetchCoursesList.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.courses = action.payload;
        });
        builder.addCase(fetchCoursesList.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(createNewCourse.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(createNewCourse.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.courses.push({ ...action.payload, students: [] });
        });
        builder.addCase(createNewCourse.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(updateCourse.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(updateCourse.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            const courseIndex = state.courses.findIndex(
                (course) => course.id === action.payload.id
            );
            if (courseIndex !== -1) {
                state.courses[courseIndex] = {
                    ...state.courses[courseIndex],
                    ...action.payload,
                };
            }
        });
        builder.addCase(updateCourse.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
    },
});

export default coursesSlice.reducer;
