import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {
    ILectorCreate,
    lectorCreate,
    getLectorsList,
    lectorUpdate,
    ILectorUpdate,
} from '../../services/lectors.service';

export interface ILector {
    id: number;
    name?: string;
    surname?: string;
    email: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface ILectorsState {
    lectors: ILector[];
    status: 'idle' | 'loading' | 'fulfilled' | 'rejected';
    error: null | string | undefined;
}

export const fetchLectorsList = createAsyncThunk<
    ILector[],
    undefined,
    { rejectValue: string }
>('lectors/fetchLectorsList', async function (_, { rejectWithValue }) {
    try {
        const res = await getLectorsList();
        return res as ILector[];
    } catch (error) {
        return rejectWithValue('Failed to fetch lectors list');
    }
});

export const createNewLector = createAsyncThunk<
    ILector,
    ILectorCreate,
    { rejectValue: string }
>(
    'lectors/createNewLector',
    async function (data: ILectorCreate, { rejectWithValue }) {
        try {
            const res = await lectorCreate(data);
            return res as ILector;
        } catch (error) {
            return rejectWithValue('Failed to create new lector');
        }
    }
);

export interface ILectorUpdateData {
    data: ILectorUpdate;
    lector: ILector;
}

export const updateLector = createAsyncThunk<
    ILector,
    ILectorUpdateData,
    { rejectValue: string }
>(
    'lectors/updateLector',
    async function (data: ILectorUpdateData, { rejectWithValue }) {
        try {
            const { name, surname, email, password } = data.data;
            const id = data.lector.id;
            const updateFields = { name, surname, email, password };
            const res = await lectorUpdate(id, updateFields);
            return res as ILector;
        } catch (error) {
            return rejectWithValue('Failed to create new lector');
        }
    }
);

const initialState = {
    lectors: [],
    status: 'idle',
    error: null,
} as ILectorsState;

const lectorsSlice = createSlice({
    name: 'lectors',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchLectorsList.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(fetchLectorsList.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.lectors = action.payload;
        });
        builder.addCase(fetchLectorsList.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(createNewLector.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(createNewLector.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.lectors.push(action.payload);
        });
        builder.addCase(createNewLector.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(updateLector.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(updateLector.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            const lectorIndex = state.lectors.findIndex(
                (lector) => lector.id === action.payload.id
            );
            if (lectorIndex !== -1) {
                state.lectors[lectorIndex] = {
                    ...state.lectors[lectorIndex],
                    ...action.payload,
                };
            }
        });
        builder.addCase(updateLector.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
    },
});

export default lectorsSlice.reducer;
