import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {
    IStudentCreate,
    IStudentUpdate,
    studentsService,
    IStudent,
} from '../../services/students.service';
import { TSortBy } from '../../Pages/Students/Students';

export interface IStudentState {
    students: IStudent[];
    status: 'idle' | 'loading' | 'fulfilled' | 'rejected';
    error: null | string | undefined;
}

export interface IStudentQuery {
    search?: string | undefined;
    sortBy: TSortBy;
}

export const fetchStudentsList = createAsyncThunk<
    IStudent[],
    IStudentQuery,
    { rejectValue: string }
>(
    'students/fetchStudentsList',
    async function (query: IStudentQuery, { rejectWithValue }) {
        try {
            let studentList;
            if (!query.search) {
                studentList = await studentsService.getStudentsList({
                    sortBy: query.sortBy,
                });
            } else {
                studentList = await studentsService.getStudentsList(query);
            }

            const updatedStudentList = await Promise.all(
                (studentList || []).map(async (student) => {
                    if (!student.imagePath) {
                        return student;
                    } else {
                        const filename = student.imagePath.split('/')[1];

                        const img = await studentsService.getStudentImg(
                            filename
                        );

                        return { ...student, imagePath: img };
                    }
                })
            );

            return updatedStudentList as IStudent[];
        } catch (error) {
            return rejectWithValue('Failed to fetch students list');
        }
    }
);

export interface IGetStudentData {
    id: number;
}

export const fetchStudentbyId = createAsyncThunk<
    IStudent,
    IGetStudentData,
    { rejectValue: string }
>(
    'students/fetchStudentbyId',
    async function (data: IGetStudentData, { rejectWithValue }) {
        try {
            const { id } = data;
            const student = await studentsService.getStudentById(id);

            if (student.imagepath) {
                const filename = student.imagepath.split('/')[1];
                const img = await studentsService.getStudentImg(filename);

                return { ...student, imagePath: img };
            }
            return student as IStudent;
        } catch (error) {
            return rejectWithValue('Failed to fetch student');
        }
    }
);

export const createNewStudent = createAsyncThunk<
    IStudent,
    IStudentCreate,
    { rejectValue: string }
>(
    'students/createNewStudent',
    async function (data: IStudentCreate, { rejectWithValue }) {
        try {
            const res = await studentsService.studentCreate(data);
            return res as IStudent;
        } catch (error) {
            return rejectWithValue('Failed to create new student');
        }
    }
);

export interface IStudentUpdateData {
    data: IStudentUpdate;
    student: IStudent;
}

export const updateStudent = createAsyncThunk<
    IStudent,
    IStudentUpdateData,
    { rejectValue: string }
>(
    'groups/updateGroup',
    async function (data: IStudentUpdateData, { rejectWithValue }) {
        try {
            const updateFields = data.data;
            const id = data.student.id;

            const res = await studentsService.studentUpdate(id, updateFields);
            return res as IStudent;
        } catch (error) {
            return rejectWithValue('Failed to update student');
        }
    }
);

const initialState = {
    students: [],
    status: 'idle',
    error: null,
} as IStudentState;

const studentsSlice = createSlice({
    name: 'students',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchStudentsList.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(fetchStudentsList.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.students = action.payload;
        });
        builder.addCase(fetchStudentsList.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(fetchStudentbyId.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(fetchStudentbyId.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            const studentIndex = state.students.findIndex(
                (student) => student.id === action.payload.id
            );
            if (studentIndex !== -1) {
                state.students[studentIndex] = {
                    ...state.students[studentIndex],
                    ...action.payload,
                };
            } else {
                state.students.push(action.payload);
            }
        });
        builder.addCase(fetchStudentbyId.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });

        builder.addCase(createNewStudent.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(createNewStudent.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.students.push(action.payload);
        });
        builder.addCase(createNewStudent.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
        builder.addCase(updateStudent.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(updateStudent.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            const studentIndex = state.students.findIndex(
                (student) => student.id === action.payload.id
            );
            if (studentIndex !== -1) {
                state.students[studentIndex] = {
                    ...state.students[studentIndex],
                    ...action.payload,
                };
            }
        });
        builder.addCase(updateStudent.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
    },
});

export default studentsSlice.reducer;
