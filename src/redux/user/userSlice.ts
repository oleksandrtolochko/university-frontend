import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getCurrentUser } from '../../services/auth.service';

export type TCurrentUser = {
    sub: number;
    username: string;
    iat: Date;
    exp: Date;
};

export const fetchCurrentUser = createAsyncThunk<
    TCurrentUser,
    void,
    { rejectValue: string }
>('user/fetchCurrentUser', async function (_, { rejectWithValue }) {
    try {
        const res = await getCurrentUser();
        return res;
    } catch (error) {
        return rejectWithValue('Failed to fetch current user');
    }
});

export interface IUsersState {
    user: TCurrentUser;
    status: 'idle' | 'loading' | 'fulfilled' | 'rejected';
    error: null | string | undefined;
}

const initialState = {
    user: {},
    status: 'idle',
    error: null,
} as IUsersState;

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchCurrentUser.pending, (state, action) => {
            state.status = 'loading';
            state.error = null;
        });
        builder.addCase(fetchCurrentUser.fulfilled, (state, action) => {
            state.status = 'fulfilled';
            state.user = action.payload;
        });
        builder.addCase(fetchCurrentUser.rejected, (state, action) => {
            state.status = 'rejected';
            state.error = action.payload;
        });
    },
});

export default userSlice.reducer;
