import { configureStore } from '@reduxjs/toolkit';
import userReducer from './user/userSlice';
import lectorsReducer from './lectors/lectorsSlice';
import coursesReducer from './courses/coursesSlice';
import groupsReducer from './groups/groupsSlice';
import studentsReducer from './students/studentsSlice';

const store = configureStore({
    reducer: {
        user: userReducer,
        lectors: lectorsReducer,
        courses: coursesReducer,
        groups: groupsReducer,
        students: studentsReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
