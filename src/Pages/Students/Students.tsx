import { ChangeEvent, useEffect, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Select from 'react-select';
import toast from 'react-hot-toast';
import debounce from 'lodash.debounce';

import { RootState } from '../../redux/store';
import { useAppDispatch, useAppSelector } from '../../redux/hooks/hooks';

import { Header, Button, Modal, Form, Input, ListItem } from '../../Components';
import styles from './students.module.scss';
import { BisnessmanIconGrey } from '../../assets/images';
import { schema } from '../../utils';
import { IInputOptions } from '../../Components/input/Input';
import { useNavigate } from 'react-router-dom';
import {
    IStudentQuery,
    createNewStudent,
    fetchStudentsList,
} from '../../redux/students/studentsSlice';
import { IStudentCreate } from '../../services/students.service';

const selectOptions = [
    { value: 'nameASC', label: 'A-Z' },
    { value: 'nameDESC', label: 'Z-A' },
    { value: 'createdAtASC', label: 'Newest first' },
    { value: 'createdAtDESC', label: 'Oldest first' },
    { value: 'all', label: 'All' },
];

export type TSortBy =
    | 'nameASC'
    | 'nameDESC'
    | 'createdAtASC'
    | 'createdAtDESC'
    | 'all';

const Students: React.FC = () => {
    const [isCreateModal, setIsCreateModal] = useState(false);
    const [search, setSearch] = useState('');
    const [sortBy, setSortBy] = useState(selectOptions[4]);
    const navigate = useNavigate();

    const dispatch = useAppDispatch();
    const studentsList = useAppSelector(
        (state: RootState) => state.students.students
    );

    useEffect(() => {
        const selectedSortOption = selectOptions.find(
            (option) => option.value === sortBy.value
        );
        if (search) {
            dispatch(
                fetchStudentsList({
                    search,
                    sortBy: selectedSortOption?.value || 'all',
                } as IStudentQuery)
            );
        } else {
            dispatch(
                fetchStudentsList({
                    sortBy: selectedSortOption?.value || 'all',
                } as IStudentQuery)
            );
        }
    }, [dispatch, search, sortBy]);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.createStudentSchema),
        criteriaMode: 'all',
    });

    const toggleCreateModal = () => {
        setIsCreateModal(!isCreateModal);
    };

    const handleCreate: SubmitHandler<IInputOptions> = async (data) => {
        try {
            dispatch(createNewStudent(data as unknown as IStudentCreate));
            toast.success('✅ New Student successfully created');
        } catch (error) {
            console.log(error);
        }
    };

    const handleSearch = async (event: ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    };

    const debouncedHandleSearch = debounce(handleSearch, 500);

    const handleSort = (event: any) => {
        setSortBy(event);
    };

    return (
        <>
            <section className={styles.students}>
                <Header title={'Students'} className={['headerAfterLine']} />
                <div className="viewContainer">
                    <div className={styles.actions}>
                        <label id="select-label">Sort by</label>
                        <Select
                            aria-labelledby="select-label"
                            className="react-select-container"
                            classNamePrefix="react-select"
                            options={selectOptions}
                            defaultValue={sortBy}
                            styles={{
                                option: (baseStyles, state) => ({
                                    ...baseStyles,
                                    backgroundColor: state.isFocused
                                        ? '#7101ff'
                                        : '#ffffff',
                                    color: '#3B4360',
                                    opacity: state.isSelected ? 1 : 0.45,

                                    ':hover': {
                                        backgroundColor: '#7101ff',
                                        color: '#ffffff',
                                    },
                                    ':focus': {
                                        backgroundColor: '#7101ff',
                                        color: '#ffffff',
                                    },
                                }),
                            }}
                            onChange={handleSort}
                        />

                        <input
                            className={styles.search}
                            type="text"
                            placeholder="Search"
                            onChange={debouncedHandleSearch}
                        />
                        <Button
                            className={[
                                'contained',
                                'flex-center',
                                'prl-4',
                                'ptb-2',
                                'br-6',
                            ]}
                            type="button"
                            onClick={() => setIsCreateModal(true)}
                            children={
                                <>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="19"
                                        height="19"
                                        viewBox="0 0 19 19"
                                        fill="none"
                                    >
                                        <path
                                            d="M8.70825 8.70834V3.95834H10.2916V8.70834H15.0416V10.2917H10.2916V15.0417H8.70825V10.2917H3.95825V8.70834H8.70825Z"
                                            fill="white"
                                        />
                                    </svg>
                                    <div style={{ marginLeft: '6px' }}>
                                        Add new student
                                    </div>
                                </>
                            }
                        />
                    </div>
                    <div className={styles.studentsView}>
                        <div className={styles.tableHeader}>
                            <div></div>
                            <div>Name</div>
                            <div>Surname</div>
                            <div>Email</div>
                            <div>Age</div>
                            <div>Course</div>
                            <div>Group</div>
                        </div>
                        {studentsList.length ? (
                            <div>
                                {studentsList.map((student) => (
                                    <div key={student.id}>
                                        <ListItem
                                            style={{
                                                gridTemplateColumns:
                                                    '10% 10% 15% 20% 10% 15% 15% 2%',
                                            }}
                                        >
                                            {student.imagePath ? (
                                                <img
                                                    className={styles.img}
                                                    src={student.imagePath}
                                                    alt="student"
                                                    width={40}
                                                    height={40}
                                                />
                                            ) : (
                                                <img
                                                    src={BisnessmanIconGrey}
                                                    alt="person"
                                                    width={40}
                                                    height={40}
                                                />
                                            )}
                                            <p className={styles.text}>
                                                {student.name}
                                            </p>

                                            <p className={styles.text}>
                                                {student.surname}
                                            </p>

                                            <p className={styles.text}>
                                                {student.email}
                                            </p>
                                            <p className={styles.text}>
                                                {student.age}
                                            </p>
                                            <p className={styles.text}>-</p>
                                            {student.groupName ? (
                                                <p className={styles.text}>
                                                    {student.groupName}
                                                </p>
                                            ) : (
                                                <p className={styles.text}>-</p>
                                            )}
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                                onClick={() =>
                                                    navigate(
                                                        `/students/${student.id}`
                                                    )
                                                }
                                            >
                                                <path
                                                    d="M16.7574 2.99666L9.29145 10.4626L9.29886 14.7097L13.537 14.7023L21 7.2393V19.9967C21 20.5489 20.5523 20.9967 20 20.9967H4C3.44772 20.9967 3 20.5489 3 19.9967V3.99666C3 3.44438 3.44772 2.99666 4 2.99666H16.7574ZM20.4853 2.09717L21.8995 3.51138L12.7071 12.7038L11.2954 12.7062L11.2929 11.2896L20.4853 2.09717Z"
                                                    fill="#BBB0C8"
                                                />
                                            </svg>
                                        </ListItem>
                                    </div>
                                ))}
                            </div>
                        ) : (
                            <p style={{ textAlign: 'center' }}>
                                Here you will see list of students. Soon ...{' '}
                            </p>
                        )}
                    </div>
                </div>
            </section>

            {isCreateModal && (
                <Modal onClose={toggleCreateModal} className={['w-600']}>
                    <h2 className={styles['create-modal__title']}>
                        Add new group
                    </h2>
                    <Form onSubmit={handleSubmit(handleCreate)}>
                        <Input
                            label="name"
                            type="text"
                            placeholder="name"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="surname"
                            type="text"
                            placeholder="surname"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="email"
                            type="email"
                            placeholder="email"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="age"
                            type="number"
                            placeholder="age"
                            register={register}
                            required
                            errors={errors}
                        />

                        <Button
                            className={['contained', 'w-100']}
                            type="submit"
                            children={'Create'}
                        />
                    </Form>
                </Modal>
            )}
        </>
    );
};

export default Students;
