import { NavLink } from 'react-router-dom';
import { Logo, Button, Input, Form } from '../../Components';
import { IInputOptions } from '../../Components/input/Input';
import styles from './forgotPassword.module.scss';
import { useForm, SubmitHandler } from 'react-hook-form';
import { schema } from '../../utils';
import { joiResolver } from '@hookform/resolvers/joi';
import { requestToChangePass } from '../../services/reset-pass.service';
import toast from 'react-hot-toast';

function ForgotPassword(): JSX.Element {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.emailSchema),
        criteriaMode: 'all',
    });

    const onSubmit: SubmitHandler<IInputOptions> = async (data) => {
        const { email } = data;
        const res = await requestToChangePass(email as string);
        if (res?.status === 201) {
            toast.success(
                'Check your email. You have 5 minutes for resetting your password'
            );
        }
    };

    return (
        <section className="page">
            <div className="container">
                <div className="box">
                    <Logo variant="primary" />
                    <h2 className="title">Reset Password</h2>
                    <p className={styles.text}>
                        Don't worry, happens to the best of us.
                        <br /> Enter the email address associated with your
                        account and we'll send you a link to reset.
                    </p>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            label="email"
                            type="email"
                            placeholder="name@mail.com"
                            register={register}
                            required
                            errors={errors}
                        />
                        <div className="btn_box">
                            <Button
                                className={['contained', 'w-100']}
                                type="submit"
                                children={'Reset'}
                            />

                            <Button
                                className={['w-100']}
                                children={
                                    <NavLink to={'/sign-in'}>Cancel</NavLink>
                                }
                            />
                        </div>
                    </Form>
                </div>
            </div>
        </section>
    );
}

export default ForgotPassword;
