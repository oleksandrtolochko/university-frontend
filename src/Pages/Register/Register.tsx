import { NavLink, useNavigate } from 'react-router-dom';
import { Logo, Button, Input, Checkbox, Form } from '../../Components';
import { IInputOptions } from '../../Components/input/Input';
import { useForm, SubmitHandler } from 'react-hook-form';
import { schema } from '../../utils';
import { joiResolver } from '@hookform/resolvers/joi';
import { usePasswordToggle } from '../../hooks';
import { userRegistration } from '../../services/auth.service';
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { Dna } from 'react-loader-spinner';

function Register(): JSX.Element {
    const [isLoading, setIsLoading] = useState(false);

    const { isShowPass, handleToggle } = usePasswordToggle();
    const navigate = useNavigate();

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.registerSchema),
        criteriaMode: 'all',
    });

    const onSubmit: SubmitHandler<IInputOptions> = async (data) => {
        const { email, password } = data;
        setIsLoading(true);

        const isRegister = await userRegistration(
            email as string,
            password as string
        );

        if (isRegister?.status === 201) {
            toast.success(isRegister.data);
            navigate('/sign-in');
            setIsLoading(false);
        }
    };

    return (
        <section className="page">
            <div className="container">
                <div className="box">
                    <Logo variant="primary" />
                    <h2 className="title">Register your account</h2>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            label="email"
                            type="email"
                            placeholder="name@mail.com"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="password"
                            type={!isShowPass ? 'password' : 'text'}
                            placeholder="set your password"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="confirm password"
                            type={!isShowPass ? 'password' : 'text'}
                            placeholder="confirm your password"
                            register={register}
                            required
                            errors={errors}
                        />

                        <Checkbox
                            label="Show Password"
                            onToggle={handleToggle}
                        />
                        <Button
                            className={['contained', 'w-100']}
                            type="submit"
                            children={'Register'}
                            disabled={isLoading}
                        />
                        {isLoading && (
                            <Dna
                                visible={true}
                                height="30"
                                width="100"
                                ariaLabel="dna-loading"
                                wrapperStyle={{}}
                                wrapperClass="dna-wrapper"
                            />
                        )}

                        <Button
                            className={['underline', 'w-100', 'shadow_link']}
                            children={
                                <NavLink to={'/sign-in'}>
                                    Already have an account?
                                </NavLink>
                            }
                        />
                    </Form>
                </div>
            </div>
        </section>
    );
}

export default Register;
