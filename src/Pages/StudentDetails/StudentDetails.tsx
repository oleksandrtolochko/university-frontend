import { useEffect, useState } from 'react';
import { Button, Form, Header, Input, Logo, Modal } from '../../Components';
import styles from './studentDetails.module.scss';
import { SubmitHandler, useForm } from 'react-hook-form';
import { IInputOptions } from '../../Components/input/Input';
import { joiResolver } from '@hookform/resolvers/joi';
import { schema } from '../../utils';
import {
    IStudentUpdateData,
    fetchStudentbyId,
    updateStudent,
} from '../../redux/students/studentsSlice';
import toast from 'react-hot-toast';
import { useAppDispatch, useAppSelector } from '../../redux/hooks/hooks';
import { useNavigate, useParams } from 'react-router-dom';
import { RootState } from '../../redux/store';
import { studentsService } from '../../services/students.service';
import { Dna } from 'react-loader-spinner';

// import Select from 'react-select';

function StudentDetails() {
    const dispatch = useAppDispatch();
    let { id } = useParams();
    const navigate = useNavigate();
    const [isReplace, setIsReplace] = useState(false);
    const [isAddModal, setIsAddModal] = useState(false);

    useEffect(() => {
        if (id !== undefined) {
            dispatch(fetchStudentbyId({ id: +id }));
        }
    }, [dispatch, id]);

    const student = useAppSelector((state: RootState) =>
        state.students.students.find(
            (student) => student.id === parseInt(id as string)
        )
    );

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.updateStudentSchema),
        criteriaMode: 'all',
    });

    const { register: registerPhoto, handleSubmit: handleSubmitPhoto } =
        useForm({
            criteriaMode: 'all',
        });

    const handleUpdate: SubmitHandler<IStudentUpdateData> = async (data) => {
        try {
            dispatch(updateStudent(data));
            toast.success('Successfully updated');
        } catch (error) {
            console.log(error);
        }
    };

    const handlePhotoUpdate = async (data: any) => {
        try {
            if (!data.file[0]) {
                toast.error('Please select a valid image file.');
                return;
            }

            const formData = new FormData();
            formData.append('file', data.file[0], data.file[0].name);

            if (!id) {
                toast.error("Student doesn't find");
                return;
            }

            const res = await studentsService.uploadPhoto(+id, formData);

            if (res) {
                toast.success('Photo successfully uploaded');
                await studentsService.getStudentImg(res);
                setIsAddModal(false);
                setIsReplace(false);
            } else {
                toast.error('Photo upload failed');
            }
        } catch (error) {
            console.error('Error uploading photo:', error);
            toast.error('An error occurred while uploading the photo');
        }
    };

    return (
        <section className={styles.section}>
            <Header className={['p-10-40', 'noneBgd']}>
                <Logo
                    variant="primary"
                    className={['m-0']}
                    width={42}
                    height={42}
                />
            </Header>

            <div className={styles['content-box']}>
                <div>
                    <Button
                        className={['flex-center']}
                        onClick={() => navigate('/layout/students')}
                    >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="25"
                            height="24"
                            viewBox="0 0 25 24"
                            fill="none"
                        >
                            <path
                                d="M16.0348 3.51501L7.5498 12L16.0348 20.485L17.4498 19.071L10.3778 12L17.4498 4.92901L16.0348 3.51501Z"
                                fill="#505E68"
                            />
                        </svg>
                        <p>Back</p>
                    </Button>
                </div>
                <div>
                    <h2 className={styles.subtitle}>Personal information</h2>
                    {student ? (
                        <>
                            <div className={styles.imgBox}>
                                {student.imagePath && !isReplace ? (
                                    <img
                                        className={styles.img}
                                        src={student.imagePath}
                                        alt="student"
                                    />
                                ) : (
                                    <div className={styles.addBtn}>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="168"
                                            height="168"
                                            viewBox="0 0 168 168"
                                            fill="none"
                                            className={styles.firstLayer}
                                        >
                                            <circle
                                                cx="84"
                                                cy="84"
                                                r="84"
                                                fill="#E4E0FF"
                                            />
                                        </svg>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="51"
                                            height="51"
                                            viewBox="0 0 51 51"
                                            fill="none"
                                            className={styles.secondLayer}
                                        >
                                            <circle
                                                cx="25.5"
                                                cy="25.5"
                                                r="25.5"
                                                fill="#7101FF"
                                            />
                                        </svg>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="32"
                                            height="32"
                                            viewBox="0 0 32 32"
                                            fill="none"
                                            className={styles.thirdLayer}
                                            onClick={() => setIsAddModal(true)}
                                        >
                                            <path
                                                d="M14.6665 14.6666V6.66663H17.3332V14.6666H25.3332V17.3333H17.3332V25.3333H14.6665V17.3333H6.6665V14.6666H14.6665Z"
                                                fill="white"
                                            />
                                        </svg>

                                        {isAddModal && (
                                            <Modal
                                                onClose={() =>
                                                    setIsAddModal(false)
                                                }
                                            >
                                                <form
                                                    onSubmit={handleSubmitPhoto(
                                                        handlePhotoUpdate
                                                    )}
                                                    encType="multipart/form-data"
                                                >
                                                    <input
                                                        type="file"
                                                        {...registerPhoto(
                                                            'file'
                                                        )}
                                                    />
                                                    <button type="submit">
                                                        Submit
                                                    </button>
                                                </form>
                                            </Modal>
                                        )}
                                    </div>
                                )}
                                <div className={styles.imgDescriptionBox}>
                                    <Button
                                        className={[
                                            'bgd-gray-100',
                                            'flex-center',
                                        ]}
                                        onClick={() => setIsReplace(true)}
                                    >
                                        <p className={styles.btnText}>
                                            Replace
                                        </p>
                                    </Button>
                                    <p className={styles.imgDescription}>
                                        Must be a .jpg or .png file smaller than
                                        10MB and at least 400px by 400px.
                                    </p>
                                </div>
                            </div>
                            <Form
                                onSubmit={handleSubmit((data) =>
                                    handleUpdate({
                                        data,
                                        student,
                                    })
                                )}
                            >
                                <Input
                                    label="name"
                                    type="text"
                                    defaultValue={student.name}
                                    register={register}
                                    required
                                    errors={errors}
                                />
                                <Input
                                    label="surname"
                                    type="text"
                                    defaultValue={student.surname}
                                    register={register}
                                    required
                                    errors={errors}
                                />
                                <Input
                                    label="email"
                                    type="email"
                                    defaultValue={student.email}
                                    register={register}
                                    required
                                    errors={errors}
                                />
                                <Input
                                    label="age"
                                    type="number"
                                    defaultValue={student.age.toString()}
                                    register={register}
                                    required
                                    errors={errors}
                                />

                                <Button
                                    className={['contained', 'w-100']}
                                    type="submit"
                                    children={'Save changes'}
                                />
                            </Form>
                        </>
                    ) : (
                        <Dna />
                    )}
                </div>
                <div>
                    <h2 className={styles.subtitle}>Courses and Groups</h2>
                    <div>
                        <h3 className={styles.blockTitle}>Course</h3>
                        {student ? (
                            student.courseName ? (
                                <p className={styles.blockInfo}>
                                    {student.courseName}
                                </p>
                            ) : (
                                <p className={styles.blockInfo}>
                                    The student has not been added to any of the
                                    courses, yet!
                                </p>
                            )
                        ) : (
                            <Dna />
                        )}
                    </div>
                    <div>
                        <h3 className={styles.blockTitle}>Group</h3>
                        {student ? (
                            student.groupName ? (
                                <p className={styles.blockInfo}>
                                    {student.groupName}
                                </p>
                            ) : (
                                <p className={styles.blockInfo}>
                                    The student has not been added to any of the
                                    groups, yet!
                                </p>
                            )
                        ) : (
                            <Dna />
                        )}
                    </div>
                </div>
            </div>
        </section>
    );
}

export default StudentDetails;
