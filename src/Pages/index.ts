import ForgotPassword from './ForgotPass/ForgotPassword';
import Login from './Login/Login';
import Register from './Register/Register';
import ResetPassword from './Reset/ResetPassword';
import Lectors from './Lectors/Lectors';
import Groups from './Groups/Groups';
import Courses from './Corses/Courses';
import Students from './Students/Students';
import StudentDetails from './StudentDetails/StudentDetails';

export {
    ForgotPassword,
    Login,
    Register,
    ResetPassword,
    Lectors,
    Groups,
    Courses,
    Students,
    StudentDetails,
};
