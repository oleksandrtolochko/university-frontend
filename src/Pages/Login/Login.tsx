import { NavLink, useNavigate } from 'react-router-dom';
import { Button, Logo, Input, Checkbox, Form } from '../../Components';
import { IInputOptions } from '../../Components/input/Input';
import styles from './login.module.scss';
import { useForm, SubmitHandler } from 'react-hook-form';
import { schema } from '../../utils';
import { joiResolver } from '@hookform/resolvers/joi';
import { usePasswordToggle } from '../../hooks';
import { userLogin } from '../../services/auth.service';
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { Dna } from 'react-loader-spinner';

function Login(): JSX.Element {
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();
    const { isShowPass, handleToggle } = usePasswordToggle();

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.loginSchema),
        criteriaMode: 'all',
    });

    const onSubmit: SubmitHandler<IInputOptions> = async (data) => {
        const { email, password } = data;
        try {
            setIsLoading(true);
            const login = await userLogin(email as string, password as string);
            if (login?.status === 201) {
                toast.success('Welcome! Very glad to see you!');

                localStorage.setItem(
                    'accessToken',
                    JSON.stringify(login.data.accessToken)
                );
                setIsLoading(false);
                navigate('/layout/lectors');
            } else {
                toast.error('Wrong email or password! Try again...');
            }
        } catch (error) {
            console.log(error);
            toast.error('Wrong email or password! Try again...');
        }
    };

    return (
        <section className="page">
            <div className="container">
                <div className="box">
                    <Logo variant="primary" />
                    <h2 className="title">Wellcome!</h2>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            label="email"
                            type="email"
                            placeholder="name@mail.com"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="password"
                            type={!isShowPass ? 'password' : 'text'}
                            placeholder="your password"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Checkbox
                            label="Show Password"
                            onToggle={handleToggle}
                        />
                        <Button
                            className={['contained', 'w-100']}
                            type="submit"
                            children={'Login'}
                            disabled={isLoading}
                        />
                        {isLoading && (
                            <Dna
                                visible={true}
                                height="30"
                                width="100"
                                ariaLabel="dna-loading"
                                wrapperStyle={{}}
                                wrapperClass="dna-wrapper"
                            />
                        )}
                        <div className={styles.navBox}>
                            <Button
                                className={[
                                    'underline',
                                    'w-100',
                                    'shadow_link',
                                ]}
                                children={
                                    <NavLink to={'/sign-up'}>
                                        Not register yet?
                                    </NavLink>
                                }
                            />

                            <Button
                                className={[
                                    'underline',
                                    'w-100',
                                    'shadow_link',
                                ]}
                                children={
                                    <NavLink to={'/forgot-password'}>
                                        Forgot password?
                                    </NavLink>
                                }
                            />
                        </div>
                    </Form>
                </div>
            </div>
        </section>
    );
}

export default Login;
