import { useState } from 'react';
import { Logo, Button, Input, Checkbox, Form } from '../../Components';
import { IInputOptions } from '../../Components/input/Input';
import styles from './resetPassword.module.scss';
import { NavLink, useSearchParams } from 'react-router-dom';
import { useForm, SubmitHandler } from 'react-hook-form';
import { schema } from '../../utils';
import { joiResolver } from '@hookform/resolvers/joi';
import { usePasswordToggle } from '../../hooks';
import { changePass } from '../../services/reset-pass.service';
import toast from 'react-hot-toast';

function ResetPassword(): JSX.Element {
    const [isReset, setIsReset] = useState(false);
    const { isShowPass, handleToggle } = usePasswordToggle();
    const [searchParams] = useSearchParams();
    const token = searchParams.get('token') as string;

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.resetPassSchema),
        criteriaMode: 'all',
    });

    const onSubmit: SubmitHandler<IInputOptions> = async (data) => {
        console.log('data: ', data);
        const newPassword = data['new password'];
        console.log('newPassword: ', newPassword);
        const res = await changePass(token, newPassword as string);
        console.log('res: ', res);

        if (res?.status === 200) {
            setIsReset(true);
            toast.success('Password successfully updated');
        }
    };

    return (
        <section className="page">
            <div className="container">
                <div className="box">
                    <Logo variant="primary" />
                    {!isReset ? (
                        <>
                            <h2 className="title">Reset Your Password</h2>
                            <Form onSubmit={handleSubmit(onSubmit)}>
                                <Input
                                    label="new password"
                                    type={!isShowPass ? 'password' : 'text'}
                                    placeholder="set new password"
                                    register={register}
                                    required
                                    errors={errors}
                                />
                                <Input
                                    label="confirm password"
                                    type={!isShowPass ? 'password' : 'text'}
                                    placeholder="confirm new password"
                                    register={register}
                                    required
                                    errors={errors}
                                />
                                <Checkbox
                                    label="Show Password"
                                    onToggle={handleToggle}
                                />
                                <Button
                                    className={['contained', 'w-100']}
                                    type="submit"
                                    children={'Reset'}
                                />
                            </Form>
                        </>
                    ) : (
                        <>
                            <h2 className="title">Password Changed</h2>
                            <p className={styles.text}>
                                You can use your new password to log into your
                                account
                            </p>
                            <div className="btn_box">
                                <Button
                                    className={['contained', 'w-100']}
                                    children={
                                        <NavLink to={'/sign-in'}>
                                            Log In
                                        </NavLink>
                                    }
                                />

                                <Button
                                    className={['w-100']}
                                    children={
                                        <NavLink to={'/'}>Go to Home</NavLink>
                                    }
                                />
                            </div>
                        </>
                    )}
                </div>
            </div>
        </section>
    );
}

export default ResetPassword;
