import { useEffect, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import toast from 'react-hot-toast';

import { useAppDispatch, useAppSelector } from '../../redux/hooks/hooks';
import { RootState } from '../../redux/store';
import {
    IGroupUpdateData,
    createNewGroup,
    fetchGroupsList,
    updateGroup,
} from '../../redux/groups/groupsSlice';

import { IGroupCreate } from '../../services/groups.service';
import { Button, Form, Header, Input, ListItem, Modal } from '../../Components';
import { IInputOptions } from '../../Components/input/Input';
import { schema } from '../../utils';

import styles from './groups.module.scss';

function Groups() {
    const [isUpdateModal, setIsUpdateModal] = useState(false);
    const [isCreateModal, setIsCreateModal] = useState(false);

    const dispatch = useAppDispatch();
    const groupsList = useAppSelector(
        (state: RootState) => state.groups.groups
    );

    useEffect(() => {
        dispatch(fetchGroupsList());
    }, [dispatch]);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.createGroupSchema),
        criteriaMode: 'all',
    });

    const {
        register: registerUpdate,
        handleSubmit: handleSubmitUpdate,
        formState: { errors: errorsUpdate },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.updateGroupSchema.optional()),
        criteriaMode: 'all',
    });

    const toggleUpdateModal = () => {
        setIsUpdateModal(!isUpdateModal);
    };

    const toggleCreateModal = () => {
        setIsCreateModal(!isCreateModal);
    };

    const handleCreate: SubmitHandler<IInputOptions> = async (data) => {
        try {
            dispatch(createNewGroup(data as unknown as IGroupCreate));
            toast.success('✅ New Group successfully created');
        } catch (error) {
            console.log(error);
        }
    };

    const handleUpdate: SubmitHandler<IGroupUpdateData> = async (data) => {
        try {
            dispatch(updateGroup(data));
            toast.success('✅ Successfully updated');
            dispatch(fetchGroupsList());
        } catch (error) {
            console.log(error);
        }
    };
    return (
        <section className={styles.groups}>
            <Header title={'Groups'} className={['headerAfterLine']} />
            <div className="viewContainer">
                <div className={styles.actions}>
                    <Button
                        className={[
                            'contained',
                            'flex-center',
                            'prl-4',
                            'ptb-2',
                            'br-6',
                        ]}
                        type="button"
                        onClick={() => setIsCreateModal(true)}
                        children={
                            <>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="19"
                                    height="19"
                                    viewBox="0 0 19 19"
                                    fill="none"
                                >
                                    <path
                                        d="M8.70825 8.70834V3.95834H10.2916V8.70834H15.0416V10.2917H10.2916V15.0417H8.70825V10.2917H3.95825V8.70834H8.70825Z"
                                        fill="white"
                                    />
                                </svg>
                                <div style={{ marginLeft: '6px' }}>
                                    Add new group
                                </div>
                            </>
                        }
                    />
                </div>
                <div className={styles.groupsView}>
                    <div className={styles.tableHeader}>
                        <div>Name</div>
                        <div>Students</div>
                    </div>
                    {groupsList.length ? (
                        <div>
                            {groupsList.map((group) => (
                                <div key={group.id}>
                                    <ListItem
                                        style={{
                                            gridTemplateColumns: '15% 80% 2%',
                                        }}
                                    >
                                        <p className={styles.text}>
                                            {group.name}
                                        </p>

                                        <p className={styles.text}>
                                            {group.students.length}
                                        </p>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            onClick={() =>
                                                setIsUpdateModal(true)
                                            }
                                        >
                                            <path
                                                d="M16.7574 2.99666L9.29145 10.4626L9.29886 14.7097L13.537 14.7023L21 7.2393V19.9967C21 20.5489 20.5523 20.9967 20 20.9967H4C3.44772 20.9967 3 20.5489 3 19.9967V3.99666C3 3.44438 3.44772 2.99666 4 2.99666H16.7574ZM20.4853 2.09717L21.8995 3.51138L12.7071 12.7038L11.2954 12.7062L11.2929 11.2896L20.4853 2.09717Z"
                                                fill="#BBB0C8"
                                            />
                                        </svg>
                                        {isUpdateModal && (
                                            <Modal
                                                onClose={toggleUpdateModal}
                                                className={['w-600']}
                                                id={group.id}
                                            >
                                                <h2
                                                    className={
                                                        styles[
                                                            'update-modal__title'
                                                        ]
                                                    }
                                                >
                                                    Edit course
                                                </h2>
                                                <Form
                                                    onSubmit={handleSubmitUpdate(
                                                        (data) =>
                                                            handleUpdate({
                                                                data,
                                                                group,
                                                            })
                                                    )}
                                                >
                                                    <Input
                                                        label="name"
                                                        type="text"
                                                        placeholder="name"
                                                        register={
                                                            registerUpdate
                                                        }
                                                        required={false}
                                                        errors={errorsUpdate}
                                                    />

                                                    <Button
                                                        className={[
                                                            'contained',
                                                            'w-100',
                                                        ]}
                                                        type="submit"
                                                        children={'Update'}
                                                    />
                                                </Form>
                                            </Modal>
                                        )}
                                    </ListItem>
                                </div>
                            ))}
                        </div>
                    ) : (
                        <p style={{ textAlign: 'center' }}>
                            Here you will see list of groups. Soon ...{' '}
                        </p>
                    )}
                </div>
                {isCreateModal && (
                    <Modal onClose={toggleCreateModal} className={['w-600']}>
                        <h2 className={styles['create-modal__title']}>
                            Add new group
                        </h2>
                        <Form onSubmit={handleSubmit(handleCreate)}>
                            <Input
                                label="name"
                                type="text"
                                placeholder="name"
                                register={register}
                                required
                                errors={errors}
                            />

                            <Button
                                className={['contained', 'w-100']}
                                type="submit"
                                children={'Create'}
                            />
                        </Form>
                    </Modal>
                )}
            </div>
        </section>
    );
}

export default Groups;
