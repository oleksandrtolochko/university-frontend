import React, { useEffect, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Select from 'react-select';
import toast from 'react-hot-toast';

import { RootState } from '../../redux/store';
import { useAppDispatch, useAppSelector } from '../../redux/hooks/hooks';
import {
    ILectorUpdateData,
    createNewLector,
    fetchLectorsList,
    updateLector,
} from '../../redux/lectors/lectorsSlice';

import { Header, Button, Modal, Form, Input, ListItem } from '../../Components';
import styles from './lectors.module.scss';
import { schema } from '../../utils';
import { IInputOptions } from '../../Components/input/Input';
import { ILectorCreate } from '../../services/lectors.service';

const selectOptions = [
    { value: 'name', label: 'A-Z' },
    { value: 'nameRevert', label: 'Z-A' },
    { value: 'createdAt', label: 'Newest first' },
    { value: 'createdAtRevert', label: 'Oldest first' },
    { value: 'all', label: 'All' },
];

const Lectors: React.FC = () => {
    const [isUpdateModal, setIsUpdateModal] = useState(false);
    const [isCreateModal, setIsCreateModal] = useState(false);

    const dispatch = useAppDispatch();
    const lectorsList = useAppSelector(
        (state: RootState) => state.lectors.lectors
    );

    useEffect(() => {
        dispatch(fetchLectorsList());
    }, [dispatch]);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.createLectorSchema),
        criteriaMode: 'all',
    });

    const {
        register: registerUpdate,
        handleSubmit: handleSubmitUpdate,
        formState: { errors: errorsUpdate },
    } = useForm<IInputOptions>({
        resolver: joiResolver(schema.updateLectorSchema.optional()),
        criteriaMode: 'all',
    });

    const toggleUpdateModal = () => {
        setIsUpdateModal(!isUpdateModal);
    };

    const toggleCreateModal = () => {
        setIsCreateModal(!isCreateModal);
    };

    const handleCreate: SubmitHandler<IInputOptions> = async (data) => {
        try {
            dispatch(createNewLector(data as unknown as ILectorCreate));
            toast.success('✅ New lector successfully created');
        } catch (error) {
            console.log(error);
        }
    };

    const handleUpdate: SubmitHandler<ILectorUpdateData> = async (data) => {
        try {
            dispatch(updateLector(data));
            toast.success('✅ Successfully updated');
            dispatch(fetchLectorsList());
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <section className={styles.lectors}>
                <Header title={'Lectors'} className={['headerAfterLine']} />
                <div className="viewContainer">
                    <div className={styles.actions}>
                        <label id="select-label">Sort by</label>
                        <Select
                            aria-labelledby="select-label"
                            className="react-select-container"
                            classNamePrefix="react-select"
                            options={selectOptions}
                            defaultValue={selectOptions[4]}
                            styles={{
                                option: (baseStyles, state) => ({
                                    ...baseStyles,
                                    backgroundColor: state.isFocused
                                        ? '#7101ff'
                                        : '#ffffff',
                                    color: '#3B4360',
                                    opacity: state.isSelected ? 1 : 0.45,

                                    ':hover': {
                                        backgroundColor: '#7101ff',
                                        color: '#ffffff',
                                    },
                                    ':focus': {
                                        backgroundColor: '#7101ff',
                                        color: '#ffffff',
                                    },
                                }),
                            }}
                        />

                        <input
                            className={styles.search}
                            type="text"
                            placeholder="Search"
                        />
                        <Button
                            className={[
                                'contained',
                                'flex-center',
                                'prl-4',
                                'ptb-2',
                                'br-6',
                            ]}
                            type="button"
                            onClick={() => setIsCreateModal(true)}
                            children={
                                <>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="19"
                                        height="19"
                                        viewBox="0 0 19 19"
                                        fill="none"
                                    >
                                        <path
                                            d="M8.70825 8.70834V3.95834H10.2916V8.70834H15.0416V10.2917H10.2916V15.0417H8.70825V10.2917H3.95825V8.70834H8.70825Z"
                                            fill="white"
                                        />
                                    </svg>
                                    <div style={{ marginLeft: '6px' }}>
                                        Add new lector
                                    </div>
                                </>
                            }
                        />
                    </div>
                    <div className={styles.lectorView}>
                        <div className={styles.tableHeader}>
                            <div>Name</div>
                            <div>Surname</div>
                            <div>Email</div>
                            <div>Password</div>
                        </div>
                        {lectorsList && (
                            <div>
                                {lectorsList.map((lector) => (
                                    <div key={lector.id}>
                                        <ListItem>
                                            {lector.name ? (
                                                <p className={styles.text}>
                                                    {lector.name}
                                                </p>
                                            ) : (
                                                <p className={styles.text}>-</p>
                                            )}
                                            {lector.surname ? (
                                                <p className={styles.text}>
                                                    {lector.surname}
                                                </p>
                                            ) : (
                                                <p className={styles.text}>-</p>
                                            )}
                                            <p className={styles.text}>
                                                {lector.email}
                                            </p>
                                            <p className={styles.text}>
                                                *********
                                            </p>
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                                onClick={() =>
                                                    setIsUpdateModal(true)
                                                }
                                            >
                                                <path
                                                    d="M16.7574 2.99666L9.29145 10.4626L9.29886 14.7097L13.537 14.7023L21 7.2393V19.9967C21 20.5489 20.5523 20.9967 20 20.9967H4C3.44772 20.9967 3 20.5489 3 19.9967V3.99666C3 3.44438 3.44772 2.99666 4 2.99666H16.7574ZM20.4853 2.09717L21.8995 3.51138L12.7071 12.7038L11.2954 12.7062L11.2929 11.2896L20.4853 2.09717Z"
                                                    fill="#BBB0C8"
                                                />
                                            </svg>
                                            {isUpdateModal && (
                                                <Modal
                                                    onClose={toggleUpdateModal}
                                                    className={['w-600']}
                                                    id={lector.id}
                                                >
                                                    <h2
                                                        className={
                                                            styles[
                                                                'update-modal__title'
                                                            ]
                                                        }
                                                    >
                                                        Edit lector
                                                    </h2>
                                                    <Form
                                                        onSubmit={handleSubmitUpdate(
                                                            (data) =>
                                                                handleUpdate({
                                                                    data,
                                                                    lector,
                                                                })
                                                        )}
                                                    >
                                                        <Input
                                                            label="name"
                                                            type="text"
                                                            placeholder="name"
                                                            register={
                                                                registerUpdate
                                                            }
                                                            required={false}
                                                            errors={
                                                                errorsUpdate
                                                            }
                                                        />
                                                        <Input
                                                            label="surname"
                                                            type="text"
                                                            placeholder="surname"
                                                            register={
                                                                registerUpdate
                                                            }
                                                            required={false}
                                                            errors={
                                                                errorsUpdate
                                                            }
                                                        />

                                                        <Input
                                                            label="email"
                                                            type="email"
                                                            placeholder="name@mail.com"
                                                            register={
                                                                registerUpdate
                                                            }
                                                            required={false}
                                                            errors={
                                                                errorsUpdate
                                                            }
                                                        />

                                                        <Input
                                                            label="password"
                                                            type="password"
                                                            placeholder="your password"
                                                            register={
                                                                registerUpdate
                                                            }
                                                            required={false}
                                                            errors={
                                                                errorsUpdate
                                                            }
                                                        />

                                                        <Button
                                                            className={[
                                                                'contained',
                                                                'w-100',
                                                            ]}
                                                            type="submit"
                                                            children={'Update'}
                                                        />
                                                    </Form>
                                                </Modal>
                                            )}
                                        </ListItem>
                                    </div>
                                ))}
                            </div>
                        )}
                    </div>
                </div>
            </section>

            {isCreateModal && (
                <Modal onClose={toggleCreateModal} className={['w-600']}>
                    <h2 className={styles['create-modal__title']}>
                        Add new lector
                    </h2>
                    <Form onSubmit={handleSubmit(handleCreate)}>
                        <Input
                            label="name"
                            type="text"
                            placeholder="name"
                            register={register}
                            required
                            errors={errors}
                        />
                        <Input
                            label="surname"
                            type="text"
                            placeholder="surname"
                            register={register}
                            required
                            errors={errors}
                        />

                        <Input
                            label="email"
                            type="email"
                            placeholder="name@mail.com"
                            register={register}
                            required
                            errors={errors}
                        />

                        <Input
                            label="password"
                            type="password"
                            placeholder="your password"
                            register={register}
                            required
                            errors={errors}
                        />

                        <Button
                            className={['contained', 'w-100']}
                            type="submit"
                            children={'Create'}
                        />
                    </Form>
                </Modal>
            )}
        </>
    );
};

export default Lectors;
